# Configuration


## Coverage

[using this to configure coverage](https://medium.com/@ulm0_/golang-multi-packages-test-coverage-with-gitlab-ci-a7b52b91ef34)

To add a coverage badge add

```
^coverage:\s(\d+(?:\.\d+)?%)
```