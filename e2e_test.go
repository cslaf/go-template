//go:build e2e
// +build e2e

package main

import "testing"

func TestHelloE2E(t *testing.T) {
	t.Run("easy e2e test",
		func(t *testing.T) {
			got := Hello()
			want := "Hello, world"

			if got != want {
				t.Errorf("got %q want %q", got, want)
			}
		})
}
